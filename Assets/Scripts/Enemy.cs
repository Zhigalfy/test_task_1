using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{

    Rigidbody enemyRB;
    NavMeshAgent agent;
    GameObject playerObject;
    PlayerController playerController;
    public float hp = 10 ;
    public float speed = 80.0f;
    public float attack = 5;   
    bool isHunter;
    bool isFollow;
    bool isEscape;
    bool isContact;


    // Start is called before the first frame update
    void Start()
    {
        enemyRB = GetComponent<Rigidbody>();
        agent = GetComponent<NavMeshAgent>();
        playerObject = GameObject.Find("Player");
        playerController = playerObject.GetComponent<PlayerController>();


    }

    // Update is called once per frame
    void FixedUpdate()
    {
        agent.speed = speed* Time.fixedDeltaTime;

        if (isContact)
        {
            isFollow = false;
            if ((transform.position - playerObject.transform.position).magnitude < 1f)
            {
                enemyRB.velocity = (transform.position - playerObject.transform.position) * speed * Time.fixedDeltaTime;
            }
            else
            {
                isContact = false;
            }
        }

        if (isFollow)
        {
            agent.destination = playerObject.transform.position;
        }
        else if (isEscape)
        {
            agent.ResetPath();
            enemyRB.velocity = (transform.position - playerObject.transform.position) * speed * Time.fixedDeltaTime;
        }

        else
        {
            agent.ResetPath();
        }



    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (!isContact)
            {
                isHunter = playerController.isHunter;
                if (!isHunter)
                {
                    isEscape = false;
                    isFollow = true;
                }

                else
                {
                    isFollow = false;
                    isEscape = true;
                }
            }
        }
    }

   

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            isFollow = false;
            isEscape = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            isContact = true;
            if (isHunter)
            {
                hp -= playerController.playerData[2];
                if(hp <= 0)
                {
                    Destroy(gameObject);
                }
            }
            
        }
    }
}



public class EnemyFast : Enemy
{
    
}

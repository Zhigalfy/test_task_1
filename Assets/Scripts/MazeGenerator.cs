using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MazeGenerator : MonoBehaviour
{
    [SerializeField] private int sizeRows = 11;
    [SerializeField] private int sizeCols = 11;
    [SerializeField] GameObject wallObject;
    [SerializeField] GameObject floorObject;
    [SerializeField] GameObject firstFloorObject;
    public NavMeshSurface surface;
    bool isFirstFloorCreated;
    public int zeroCounter { get; private set; } = 0;

    private int[,] maze;
    public int[,] Maze
        {
        get { return maze; }
        private set { maze = value; }
        }

private void Awake()
    {
        
       Maze = GenerateMaze(sizeRows, sizeCols);
        ConstructMaze();
        surface.BuildNavMesh();
    }

    private void Start()
    {
        
    }

    int[,] GenerateMaze(int sizeRows, int sizeCols)
    {
        int[,] maze = new int[sizeRows, sizeCols];
        int rMax = maze.GetUpperBound(0);
        int cMax = maze.GetUpperBound(1);

        
        for (int i = 0; i <= rMax; i++)
        {
            
            for (int j = 0; j <= cMax; j++)
            {
                
                if (i == 0 || j == 0 || i == rMax || j == cMax)
                {
                    maze[i, j] = 1;
                }

                
                else if (i % 2 == 0 && j % 2 == 0)
                {
                    
                    maze[i, j] = 1;

                    if (Random.value > 0.01f)
                    {
                        int a = Random.value < 0.5f ? 0 : (Random.value < 0.5f ? -1 : 1);
                        int b = a != 0 ? 0 : (Random.value < 0.5f ? -1 : 1);
                        maze[i + a, j + b] = 1;
                    }
                }
                                
            }
                        
        }

        return maze;

    }

    private void ConstructMaze()
    {
        int rMax = maze.GetUpperBound(0);
        int cMax = maze.GetUpperBound(1);


        for (int i = 0; i <= rMax; i++)
        {
            for (int j = 0; j <= cMax; j++)
            {

                if (maze[i, j] == 0)
                {
                    zeroCounter++;
                    if (!isFirstFloorCreated)
                    {
                        Instantiate(firstFloorObject, new Vector3(i, 0, j), Quaternion.identity);
                        isFirstFloorCreated = true;
                    }
                    else
                    {
                        Instantiate(floorObject, new Vector3(i, 0, j), Quaternion.identity);
                    }
                }


                else if (maze[i, j] == 1)
                {

                    Instantiate(wallObject, new Vector3(i, 0.5f, j), Quaternion.identity);


                }
            }
        }
    }
}

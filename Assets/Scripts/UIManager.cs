using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIManager : MonoBehaviour
{
    [SerializeField] GameObject panel;
    [SerializeField] Canvas joystick;
    [SerializeField] Button startButton;
    [SerializeField] Button pauseButton;
    [SerializeField] Button ExitButton;
    [SerializeField] Text titleText;
    [SerializeField] Text pauseText;
    [SerializeField] Text playerData;
    [SerializeField] PlayerController Controller;

    void Start()
    {
        
        panel.transform.position = new Vector2(Screen.width / 2, Screen.height / 2);
        

        titleText.gameObject.SetActive(true);
        titleText.text = "MAZE GAME";
        startButton.gameObject.SetActive(true);
        pauseButton.gameObject.SetActive(false);
        ExitButton.gameObject.SetActive(true);
        joystick.gameObject.SetActive(false);
        Time.timeScale = 0f;

    }

    
    void Update()
    {
        playerData.text = $"Speed: {Controller.playerData[0]}\nHP: {Controller.playerData[1]}\nAttack: {Controller.playerData[2]}";
    }

    public void StartGame()
    {
        titleText.gameObject.SetActive(false);       
        startButton.gameObject.SetActive(false);
        pauseButton.gameObject.SetActive(true);
        ExitButton.gameObject.SetActive(true);
        joystick.gameObject.SetActive(true);
        panel.transform.position = new Vector2(panel.GetComponent<RectTransform>().rect.width / 2, panel.GetComponent<RectTransform>().rect.height / 2);
        Time.timeScale = 1f;
    }

    public void PauseGame()
    {
        titleText.gameObject.SetActive(true);
        titleText.text = "PAUSE";
        startButton.gameObject.SetActive(true);
        pauseButton.gameObject.SetActive(false);
        ExitButton.gameObject.SetActive(true);
        joystick.gameObject.SetActive(false);
        Time.timeScale = 0f;
    }

    public void ExitGame()
    {
#if UNITY_EDITOR

        UnityEditor.EditorApplication.ExitPlaymode();

#endif
        Application.Quit();
    }
    
    public void GameOver()
    {
        titleText.gameObject.SetActive(true);
        titleText.text = "Game over";
        startButton.gameObject.SetActive(false);
        pauseButton.gameObject.SetActive(false);
        ExitButton.gameObject.SetActive(true);
        joystick.gameObject.SetActive(false);
        Time.timeScale = 0f;
    }
    private void OnApplicationFocus(bool focus)
    {
        if (!focus)
        {
            PauseGame();
        }
    }

}

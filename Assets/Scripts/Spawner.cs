using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    MazeGenerator generator;
    [SerializeField] GameObject player;
    [SerializeField] GameObject enemy;
    [SerializeField] GameObject pickup;
    [Header("Number of enemies or less")] 
    [SerializeField] int enemyCount;
    int[,] maze;
    int floorCount;
    Vector2 playerPos;


    // Start is called before the first frame update
    void Start()
    {
        generator = GetComponent<MazeGenerator>();
        maze = generator.Maze;
        floorCount = generator.zeroCounter;
        SpawnPlayer();
        SpawnEnemyAndPickup(enemy);
        SpawnEnemyAndPickup(pickup);

    }

    void SpawnPlayer()
    {        
        int rMax = maze.GetUpperBound(0);
        int cMax = maze.GetUpperBound(1);

        for (int i = 0; i <= rMax; i++)
        {

            for (int j = 0; j <= cMax; j++)
            {

                if (maze[i, j] == 0 )
                {
                    player.SetActive(true);
                    player.transform.position = new Vector3(i, 0.25f, j);
                    playerPos = new Vector2(i, j);
                    return;
                }                              

            }

        }
    }

    void SpawnEnemyAndPickup(GameObject gameObject)
    {
        int rMax = maze.GetUpperBound(0);
        int cMax = maze.GetUpperBound(1);
        float spawnChance = ( (float) enemyCount / (float) floorCount);
        int counter =0;
        

        for (int i = 0; i <= rMax; i++)
        {

            for (int j = 0; j <= cMax; j++)
            {

                if (maze[i, j] == 0 && (maze[i+1,j] == 0 || maze[i-1,j] == 0 || maze[i, j+1] == 0 || maze[i, j-1] == 0) && (i != playerPos.x || j != playerPos.y ))
                {
                    
                    if (Random.value < spawnChance && counter< enemyCount)
                    {
                        
                        Instantiate(gameObject, new Vector3(i, 0.25f, j), Quaternion.identity);
                        counter++;
                    }
                                        
                }

            }

        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}

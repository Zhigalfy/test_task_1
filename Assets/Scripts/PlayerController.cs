using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] GameObject cam;
    [SerializeField] GameObject joystickBG;
    [SerializeField] GameObject joystick;
    Rigidbody playerRB;
    [SerializeField] private bool isJoystick;
    float playerSpeed  = 100.0f;
    float playerHP  = 20.0f;
    float playerAttack  = 10.0f;
    public List<float> playerData = new List<float>();
    [SerializeField] UIManager UI;
    Vector3 moveDir;
    Vector2 startPos;
    bool isDragging;
    float joyWidth;
    public bool isHunter { get; private set; } 
    

    // Start is called before the first frame update
    void Start()
    {
        playerRB = GetComponent<Rigidbody>();
        joystickBG.gameObject.SetActive(isJoystick);        
        joyWidth = joystickBG.GetComponent<RectTransform>().rect.width;
        playerData.Add(playerSpeed);
        playerData.Add(playerHP);
        playerData.Add(playerAttack);
        


    }

    // Update is called once per frame
    void Update()
    {
        if (!isJoystick)
        {
            KeyboardController();            
        }
        else
        {
            JoystickController();
        }
       
    }
    private void FixedUpdate()
    {
        playerRB.velocity = moveDir * playerSpeed * Time.fixedDeltaTime;
        
    }

    private void LateUpdate()
    {
        cam.transform.position = transform.position + new Vector3(0f, 10f, 0f);
    }

    void KeyboardController()
    {
        float hPos = Input.GetAxis("Horizontal");
        float vPos = Input.GetAxis("Vertical");
        moveDir = new Vector3(hPos, 0f, vPos).normalized;
    }
    void JoystickController()
    {
        moveDir = Vector3.zero;
        if (Input.GetMouseButtonDown(0))
        {
            startPos = Input.mousePosition;
            joystickBG.transform.position = startPos;
            isDragging = true;
        }
        else if (Input.GetMouseButton(0) && isDragging)
        {
            Vector2 pos = Input.mousePosition;
            Vector2 deltaDir = (pos - startPos).normalized;
            float deltaDist = Vector2.Distance(startPos, pos);
            if (deltaDist > joyWidth / 2)
            {
                deltaDist = joyWidth / 2;
            }

            moveDir = new Vector3(deltaDir.x, 0f, deltaDir.y);
            joystick.transform.position = joystickBG.transform.position + new Vector3(deltaDir.x, deltaDir.y) * deltaDist;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            isDragging = false;
            moveDir = Vector3.zero;
            joystick.transform.position = joystickBG.transform.position;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            if (!isHunter)
            {
                StartCoroutine(Hunter());
                isHunter = true;
                Destroy(other.gameObject);
            }
        }
    }

    private IEnumerator Hunter()
    {
        yield return new WaitForSeconds(10f);
        isHunter = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (!isHunter)
            {
                playerData[1] -= collision.gameObject.GetComponent<Enemy>().attack;
                if(playerData[1] <= 0)
                {
                    UI.GameOver();
                }
            }
        }
    }
}
